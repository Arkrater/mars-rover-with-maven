package Main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import ClassPlateau.Plateau;
import ClassRover.Rover;

public class Main {

	public static void main(String[] args) throws IOException {
		roverCoordenatesAndMovement();
	}

	@SuppressWarnings("unlikely-arg-type")
	public static void roverCoordenatesAndMovement() throws IOException {

		Plateau plateau = new Plateau();

		try (BufferedWriter   writer = new BufferedWriter  (new FileWriter("./coordenadasWithMaven.csv"))) {
			StringBuilder plateauContent = new StringBuilder();
			StringBuilder coordenatesContent = new StringBuilder();

			int inputDimensionPlateau = 2;
			for (int i = 1; i <= inputDimensionPlateau; i++) {
				while (plateau.getSizeX() == 0) {
					System.out.println("Input Plateau Dimension X please");

					Scanner sc = new Scanner(System.in);
					if (sc.hasNextInt()) {
						plateau.setSizeX(sc.nextInt());

						break;
					} else {
						System.out.println("please input a number");
					}

				}

				while (plateau.getSizeY() == 0) {
					System.out.println("Input Plateau Dimension Y please");

					Scanner sc = new Scanner(System.in);
					if (sc.hasNextInt()) {
						plateau.setSizeY(sc.nextInt());
						break;
					} else {
						System.out.println("please input a number");
					}

				}

			}

			System.out.println("Plateau Size is: " + plateau.getSizeX() + "," + plateau.getSizeY());
			plateauContent.append(plateau.getSizeX() + "," + plateau.getSizeY() + "\n");

			writer.write(plateauContent.toString());

			Rover rover = new Rover();
			Rover rover2 = new Rover();

			roverSetup(rover);

			coordenatesContent
					.append(rover.getPositionX() + "," + rover.getPositionY() + "," + rover.getOrientation() + "\n");

			writer.write(coordenatesContent.toString());

			System.out.println(" Rover Movement is: " + rover.getMovement());

			writer.write(rover.getMovement() + "\n");

			roverSetup(rover2);

			coordenatesContent
					.append(rover2.getPositionX() + "," + rover2.getPositionY() + "," + rover2.getOrientation() + "\n");

			writer.write(coordenatesContent.toString());

			System.out.println(" Rover2 Movement is: " + rover2.getMovement());

			writer.write(rover2.getMovement() + "\n");

			calculateMovement(rover);
			calculateMovement(rover2);

			if ((rover.getPositionX() > plateau.getSizeX()) || rover.getPositionY() > plateau.getSizeY()) {
				System.out.println("Rover1 is out range!");
			}

			if ((rover2.getPositionX() > plateau.getSizeX()) || rover2.getPositionY() > plateau.getSizeY()) {
				System.out.println("Rover2 is out range!");
			}

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	public static void calculateMovement(Rover rover) {
		String movement = rover.getMovement();
		String currentOrientation = rover.getOrientation();
		int nOrientationForL = calculatedOrientation(currentOrientation);
		int nOrientationForR = calculatedOrientation(currentOrientation);
		int currentValueRoverY = rover.getPositionY();
		int currentValueRoverX = rover.getPositionX();

		System.out.println("current value of Rover: " + rover.getPositionX() + "," + rover.getPositionY() + ","
				+ rover.getOrientation());

		// Creating array of string length
		char[] ch = new char[movement.length()];

		// Copy character by character into array
		for (int i = 0; i < movement.length(); i++) {
			ch[i] = movement.charAt(i);
		}

		// Printing content of array
		for (char c : ch) {
			String cToString = Character.toString(c);
			switch (cToString) {
			case "L":
				// System.out.println("enter case for L");

				nOrientationForL--;
				if (nOrientationForL == 4) {
					// System.out.println("its orientation is: 4");
					rover.setOrientation("N");
				}
				if (nOrientationForL == 3) {
					// System.out.println("its orientation is: 3");
					rover.setOrientation("W");
				}
				if (nOrientationForL == 2) {
					// System.out.println("its orientation is: 2");
					rover.setOrientation("S");
				}
				if (nOrientationForL == 1) {
					// System.out.println("its orientation is: 1");
					rover.setOrientation("E");
				}
				if (nOrientationForL == 0) {
					nOrientationForL = 4;
					// System.out.println("its orientation is reset to 4");
					rover.setOrientation("N");
				}
				break;
			case "R":
				// System.out.println("enter case for R");

				nOrientationForR++;
				if (nOrientationForR == 1) {
					// System.out.println("its orientation is: 1");
					rover.setOrientation("E");
				}
				if (nOrientationForR == 2) {
					// System.out.println("its orientation is: 2");
					rover.setOrientation("S");
				}
				if (nOrientationForR == 3) {
					// System.out.println("its orientation is: 3");
					rover.setOrientation("W");
				}
				if (nOrientationForR == 4) {
					// System.out.println("its orientation is: 4");
					rover.setOrientation("N");
				}
				if (nOrientationForR == 5) {
					nOrientationForR = 1;
					rover.setOrientation("E");
				}
				break;
			case "M":
				// System.out.println("enter case for M");
				String switchOrientation = rover.getOrientation();
				switch (switchOrientation) {
				case "N":
					currentValueRoverY++;
					rover.setPositionY(currentValueRoverY);
					break;
				case "W":
					currentValueRoverX--;
					rover.setPositionX(currentValueRoverX);
					break;
				case "S":
					currentValueRoverY--;
					rover.setPositionY(currentValueRoverY);
					break;
				case "E":
					currentValueRoverX++;
					rover.setPositionX(currentValueRoverX);
					break;
				}
				break;

			}

		}
		System.out.println("New Rovers configuration is: : " + rover.getPositionX() + "," + rover.getPositionY() + ","
				+ rover.getOrientation());
	}

	public static int calculatedOrientation(String currentOrientation) {

		int orientationValue = 0;
		if (currentOrientation.equals("N")) {
			orientationValue = 4;
		}
		if (currentOrientation.equals("W")) {
			orientationValue = 3;
		}
		if (currentOrientation.equals("S")) {
			orientationValue = 2;
		}
		if (currentOrientation.equals("E")) {
			orientationValue = 1;
		}
		return orientationValue;
	}

	public static void roverSetup(Rover rover) {

		int coordenates = 3;
		for (int i = 1; i <= coordenates; i++) {
			while (rover.getPositionX() == 0) {
				System.out.println("Input Rover X Axis please");

				Scanner sc = new Scanner(System.in);
				if (sc.hasNextInt()) {
					rover.setPositionX(sc.nextInt());
					break;
				} else {
					System.out.println("please input a number");
				}

			}

			while (rover.getPositionY() == 0) {
				System.out.println("Input Rover Y Axis please");

				Scanner sc = new Scanner(System.in);
				if (sc.hasNextInt()) {
					rover.setPositionY(sc.nextInt());
					break;
				} else {
					System.out.println("please input a number");
				}

			}

			while (rover.getOrientation() == null) {
				System.out.println("Input Rover Orientation please, should be N, W, S or E");

				Scanner sc = new Scanner(System.in);
				String direction = sc.next().toUpperCase();
				switch (direction) {
				case "N":
					rover.setOrientation(direction);
					break;
				case "W":
					rover.setOrientation(direction);
					break;
				case "S":
					rover.setOrientation(direction);
					break;
				case "E":
					rover.setOrientation(direction);
					break;
				default:
					System.out.println("please input a direction");
				}

			}

		}

		System.out.println(" Rover Position is: " + rover.getPositionX() + "," + rover.getPositionY() + ","
				+ rover.getOrientation());

		// coordenatesContent.append(rover.getPositionX() + "," + rover.getPositionY() +
		// "," + rover.getOrientation() + "\n");

		// writer.write(coordenatesContent.toString());

		String movementInput = null;
		StringBuilder movementContent = new StringBuilder();
		System.out.println(
				"Input Rover Movement please, should be L for Left, R for Right, M for Move or Exit to save Movement");
		while (rover.getMovement() == null) {

			Scanner sc = new Scanner(System.in);
			movementInput = sc.next().toUpperCase();
			switch (movementInput) {
			case "L":
				movementContent.append(movementInput);
				System.out.println("Current movement: " + movementContent + " - add another movement or exit");
				break;
			case "R":
				movementContent.append(movementInput);
				System.out.println("Current movement: " + movementContent + " - add another movement or exit");
				break;
			case "M":
				movementContent.append(movementInput);
				System.out.println("Current movement: " + movementContent + " - add another movement or exit");
				break;
			case "EXIT":
				rover.setMovement(movementContent.toString());
				break;
			default:
				System.out.println("please input a valid movement: [L, R, M]");
			}
		}

	}

}
